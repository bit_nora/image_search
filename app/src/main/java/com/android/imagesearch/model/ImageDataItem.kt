package com.android.imagesearch.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageResponse(
    val _type: String,
    val totalCount: Int,
    val value: List<Image>)

@JsonClass(generateAdapter = true)
@Entity
data class Image(
    val imageWebSearchUrl: String,
    val title: String,
    val name: String?,
    val base64Encoding: String?,
    val thumbnailWidth: Int,
    val thumbnailHeight: Int,
    val thumbnail: String,
    val width: Int,
    val height: Int,
    @PrimaryKey
    val url: String
) {
}

//"_type": "images",
//"totalCount": 2404,
//"value": [
//{
//    "url": "https://sm.mashable.com/t/mashable_sea/review/m/miss-ameri/miss-americana-wont-change-your-mind-about-taylor-swift-but_348c.1200.jpg",
//    "height": 675,
//    "width": 1200,
//    "thumbnail": "https://rapidapi.contextualwebsearch.com/api/thumbnail/get?value=6686564304841445937",
//    "thumbnailHeight": 252,
//    "thumbnailWidth": 448,
//    "base64Encoding": null,
//    "name": "",
//    "title": "'Miss Americana' won't change your mind about Taylor Swift, but it's not trying to, either - Entertainment",
//    "imageWebSearchUrl": "https://contextualwebsearch.com/search/taylor%20swift/images"
//}]