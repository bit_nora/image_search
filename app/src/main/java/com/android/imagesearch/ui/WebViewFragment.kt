package com.android.imagesearch.ui

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.webkit.WebViewClientCompat
import com.android.imagesearch.R
import com.android.imagesearch.databinding.FragmentWebviewBinding

class WebViewFragment : Fragment() {

    private lateinit var binding: FragmentWebviewBinding

    private val enableJs = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_webview,
                container,
                false)

        binding.lifecycleOwner = this

        val args = WebViewFragmentArgs.fromBundle(requireArguments())
        loadWebView(args.webUrl)

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun loadWebView(url: String){

        binding.wvShow.settings.loadWithOverviewMode = true
        binding.wvShow.settings.setSupportZoom(true)
        binding.wvShow.settings.displayZoomControls = false
        binding.wvShow.settings.builtInZoomControls = true
        binding.wvShow.settings.useWideViewPort = true
        binding.wvShow.setInitialScale(1)
        binding.wvShow.settings.javaScriptEnabled = enableJs

        binding.wvShow.loadUrl(url)
        binding.wvShow.webViewClient = object : WebViewClientCompat() {

            override fun onPageFinished(view: WebView?, url: String?) {
                Log.d("onPageFinished", "$url")
                binding.ctpgLoadingWebPage.hide()
            }
        }
    }
}