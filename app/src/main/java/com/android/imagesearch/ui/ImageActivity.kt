package com.android.imagesearch.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.android.imagesearch.R
import com.android.imagesearch.databinding.ActivityImageBinding

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Add up button in the app bar
        val binding = DataBindingUtil.setContentView<ActivityImageBinding>(this, R.layout.activity_image)

        val navController = this.findNavController(R.id.image_navigation)

        setSupportActionBar(binding.toolbar)

        // then setup the action bar, tell it about the DrawerLayout
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.image_navigation)
        return navController.navigateUp()
    }
}
