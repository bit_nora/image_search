package com.android.imagesearch.ui

import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.imagesearch.R
import com.android.imagesearch.databinding.ImageItemBinding
import com.android.imagesearch.model.Image

class ImageAdapter (private val listener: OnClickListener) : ListAdapter<Image, RecyclerView.ViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.image_item,
                parent,
                false)
        )
    }

    class ImageViewHolder(private val binding: ImageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Image, listener: OnClickListener) {
            binding.apply {
                image = item

                ivThumbnail.setTag(R.id.rvImage, item.url)

                val content = SpannableString("Search In Web")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                tvSearchInWeb.text = content
                tvSearchInWeb.tag = item.imageWebSearchUrl
                executePendingBindings()
            }

            binding.ivThumbnail.setOnClickListener {
                listener.onClick(it)
            }

            binding.tvSearchInWeb.setOnClickListener{
                listener.onClick(it)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val imageItem = getItem(position)
        (holder as ImageViewHolder).bind(imageItem, listener)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Image>() {
        override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem == newItem
        }
    }

    interface OnClickListener {
        fun onClick(view: View)
    }
}

