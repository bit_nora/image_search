package com.android.imagesearch.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.imagesearch.R
import com.android.imagesearch.databinding.FragmentImageviewDetailBinding
import com.android.imagesearch.viewmodels.ImageViewModel
import com.android.imagesearch.viewmodels.ViewModelFactory

class ImageViewFragment : DialogFragment() {

    private lateinit var binding: FragmentImageviewDetailBinding

    private var imageUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            imageUrl = requireArguments().getString("url").toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_imageview_detail,
                container,
                false)

        binding.lifecycleOwner = this
        binding.imageUrl = imageUrl

        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(true);

        binding.root.setOnClickListener{
            dismiss()
        }

        Log.d("ImageViewFragment","ImageViewFragment has called $imageUrl" )

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        val dialog = dialog
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    companion object {
        @JvmStatic
        fun newInstance(imageUrl: String) =
            ImageViewFragment().apply {
                arguments = Bundle().apply {
                    putString("url", imageUrl)
                }
            }
    }
}