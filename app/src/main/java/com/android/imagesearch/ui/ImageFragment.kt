package com.android.imagesearch.ui

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.imagesearch.R
import com.android.imagesearch.databinding.FragmentImageBinding
import com.android.imagesearch.viewmodels.ImageViewModel
import com.android.imagesearch.viewmodels.ViewModelFactory

class ImageFragment : Fragment() {

    private val viewModel: ImageViewModel by lazy{
        val activity = requireNotNull(this.activity)
        ViewModelProvider(this, ViewModelFactory(activity.application)).get(ImageViewModel::class.java)
    }

    private lateinit var binding: FragmentImageBinding

    private lateinit var imageAdapter: ImageAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_image,
                container,
                false)

        binding.lifecycleOwner = this
        binding.rvImage.layoutManager = LinearLayoutManager(context)

        // add dividers between RecyclerView's row items
        val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        binding.rvImage.addItemDecoration(decoration)

        setupScrollListener()

        initAdapter()

        val query = viewModel.currentQueryData?: DEFAULT_QUERY
        initSearch(query)

        viewModel.status.observe(viewLifecycleOwner, Observer {
            when(it.first){
                ImageViewModel.STATE_INITIAL ->{
                    binding.pbSpinner.isVisible = true
                    Log.i("onCreateView", "STATE_INITIAL")
                }
                ImageViewModel.STATE_LOADING ->{
                    Log.i("onCreateView", "STATE_LOADING")
                    if(imageAdapter.itemCount == 0)
                        binding.pbSpinner.isVisible = true
                    else
                        binding.pbSpinnerFetchMore.visibility = View.VISIBLE
                }
                ImageViewModel.STATE_SUCCESS ->{
                    Log.i("onCreateView", "STATE_SUCCESS")
                    binding.pbSpinner.isVisible = false
                    showEmptyList(imageAdapter.itemCount == 0)
                    binding.pbSpinnerFetchMore.visibility = View.GONE
                }
                ImageViewModel.STATE_ERROR ->{
                    //check the error status and prompt
                    Log.i("onCreateView", "STATE_ERROR")
                    binding.pbSpinner.isVisible = false
                    binding.pbSpinnerFetchMore.visibility = View.GONE
                    val error = it.second as Throwable
                    showAlert(getString(R.string.error_msg), error.localizedMessage?: getString(R.string.error_description))
                    showEmptyList(imageAdapter.itemCount == 0)
                }
            }
        })

        return binding.root
    }

    private fun initAdapter() {
        imageAdapter = ImageAdapter (object : ImageAdapter.OnClickListener {
            override fun onClick(view: View) {
                val viewTag = view.getTag(R.id.rvImage) ?: view.tag
                val url = viewTag as String
                Log.d("Web URL", "$url")
                when (view.id) {
                    R.id.ivThumbnail -> {
                        ImageViewFragment.newInstance(url)
                            .show(childFragmentManager, "")
                    }
                    R.id.tvSearchInWeb -> {
                        navigateToWebView(url)
                    }
                }

            }
        })

        binding.rvImage.adapter = imageAdapter
        viewModel.images.observe(viewLifecycleOwner, Observer {imageList ->
            imageAdapter.submitList(imageList).let {
                if (imageList.isEmpty() && viewModel.status.value?.first != ImageViewModel.STATE_LOADING) {
                    showEmptyList(true)
                } else {
                    showEmptyList(false)
                }
            }
        })
    }

    private fun navigateToWebView(
        url: String
    ) {
        val action =
            ImageFragmentDirections.actionImageFragmentToWebViewFragment(url)
        findNavController().navigate(action)
    }

    private fun initSearch(query: String) {
        binding.etSearch.setText(query)
        updateRepoListFromInput()

        binding.etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
        binding.etSearch.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
    }

    private fun updateRepoListFromInput() {
        binding.etSearch.text.trim().let {
            if (it.isNotEmpty()) {
                binding.rvImage.scrollToPosition(0)
                viewModel.searchImage(it.toString())
            }
        }
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            binding.emptyList.visibility = View.VISIBLE
            binding.rvImage.visibility = View.GONE
        } else {
            binding.emptyList.visibility = View.GONE
            binding.rvImage.visibility = View.VISIBLE
        }
    }

    private fun setupScrollListener() {
        val layoutManager = binding.rvImage.layoutManager as LinearLayoutManager
        binding.rvImage.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }

    private fun showAlert(title: String, msg: String) {
        val alertDialogBuilder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(
            requireContext()
        )
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder
            .setMessage(msg)
            .setCancelable(false)
            .setNeutralButton("OK",

        DialogInterface.OnClickListener { dialog, id -> })

        if (activity?.isFinishing == false) {
            alertDialogBuilder.create().show()
        }
    }

    companion object {
        private const val DEFAULT_QUERY = "Taylor Swift"
    }
}