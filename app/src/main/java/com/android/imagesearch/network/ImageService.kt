package com.android.imagesearch.network

import com.android.imagesearch.BuildConfig
import com.android.imagesearch.model.ImageResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface ImageService {
    /*https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI?autoCorrect=true&pageNumber=10&
     pageSize=10&safeSearch=false&q=Taylor Swift*/

    @GET("api/Search/ImageSearchAPI")
    suspend fun search(
//        @Header("x-rapidapi-key") apiKey: String,
        @Query("autoCorrect") autoCorrect: Boolean,
        @Query("pageNumber") pageNumber: Int,
        @Query("pageSize") pageSize: Int,
        @Query("safeSearch") safeSearch: Boolean,
        @Query("q") q: String
    ): Response<ImageResponse>
}

object ImageRetrofitService {
    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .apply {
        addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        addInterceptor(
            Interceptor { chain ->
                val builder = chain.request().newBuilder()
                builder.header("x-rapidapi-key", BuildConfig.IMAGE_API_KEY)
                return@Interceptor chain.proceed(builder.build())
            }
        )
    }.build()

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://contextualwebsearch-websearch-v1.p.rapidapi.com/")
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .build()

    val IMAGE_REST_ADAPTER: ImageService = retrofit.create(
        ImageService::class.java)
}


