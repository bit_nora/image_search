package com.android.imagesearch.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.imagesearch.database.ImageDB
import com.android.imagesearch.model.Image
import com.android.imagesearch.network.ImageRetrofitService
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ImageViewModel(application: Application) : AndroidViewModel(application) {

    private val imageDB = ImageDB.getInstance(application)
    private val imageService = ImageRetrofitService.IMAGE_REST_ADAPTER
    private val imageDao = imageDB.imageDao

    private var lastRequestedPage = STARTING_PAGE_INDEX
    private var isRequestInProgress = false
    var currentQueryData: String? = null

    private val _status = MutableLiveData<Pair<Int, Any?>>(Pair(STATE_INITIAL, null))
    val status: LiveData<Pair<Int, Any?>>
        get() = _status

    private val _images = imageDao.selectAll()
    val images: LiveData<List<Image>>
        get() = _images

    var image: Image? = null

    fun searchImage(query: String){
        _status.postValue(Pair(STATE_LOADING, null))

        val coroutineExceptionHandler = CoroutineExceptionHandler{_, e ->
            Log.e("searchMovie: ", e.localizedMessage.toString())
            _status.postValue(Pair(STATE_ERROR, e))
        }

        viewModelScope.launch (coroutineExceptionHandler + Dispatchers.IO){
            getSearchImages(query)
            currentQueryData = query
            _status.postValue(Pair(STATE_SUCCESS, null))
        }
    }

    // Fetch images and save to local cache
    private suspend fun getSearchImages(query: String){
        lastRequestedPage = STARTING_PAGE_INDEX

        // Check if query is the same as current search
        // If same query, let listScrolled handle fetching more data
        if(query == currentQueryData)
            return

        //delete search data from local cache every new search request since old search result is not required
        imageDao.deleteAll()
        if (requestAndSaveImages(query)) {
            lastRequestedPage++
        }
    }

    // Fetch images from network and save into local cache
    private suspend fun requestAndSaveImages(query: String): Boolean {
        isRequestInProgress = true
        var successful = false

        val response = imageService.search(true, lastRequestedPage, PAGE_SIZE, true, query)
        if(response.isSuccessful){
            val images = response.body()?.value
            if (images != null) {
                val imageSize = imageDao.insertAll(images)
                Log.i("save into local db ", imageSize.size.toString())

                successful = imageSize.size == images.size
            }
        }else{
            throw RuntimeException(response.errorBody()?.toString())
        }

        isRequestInProgress = false
        return successful
    }

    //Request more when we scroll down
    private suspend fun requestMore(query: String) {
        Log.d("requestMore", "requestMore is called")
        //to avoid the user search multi times when i search in progress
        if (isRequestInProgress) return

        _status.postValue(Pair(STATE_LOADING, null))
        Log.d("requestMore: ", "STATE_LOADING")

        val successful = requestAndSaveImages(query)
        if (successful) {
            lastRequestedPage++

            _status.postValue(Pair(STATE_SUCCESS, null))
            Log.d("listScrolled: ", "STATE_SUCCESS")
        }
    }

    fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        Log.i("listScrolled",
            "visibleItemCount: $visibleItemCount,lastVisibleItemPosition :$lastVisibleItemPosition, totalItemCount: $totalItemCount"
        )
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount) {
            if (currentQueryData != null) {
                val coroutineExceptionHandler = CoroutineExceptionHandler{_, e ->
                    Log.e("listScrolled: ", e.localizedMessage.toString())
                    _status.postValue(Pair(STATE_ERROR, e))
                }

                viewModelScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
                    requestMore(currentQueryData!!)
                }
            }
        }
    }

    companion object {
        const val STATE_INITIAL = 1
        const val STATE_LOADING = 2
        const val STATE_SUCCESS = 3
        const val STATE_ERROR = 4
        private const val VISIBLE_THRESHOLD = 5

        private const val PAGE_SIZE = 20
        private const val STARTING_PAGE_INDEX = 1
    }
}
