package com.android.imagesearch.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.android.imagesearch.model.Image

@Database(entities = [Image:: class], version = 1)
abstract class ImageDB : RoomDatabase(){

    abstract val imageDao : ImageDao

    companion object{
        private val sLock = Object()
        private var INSTANCE: ImageDB? = null

        fun getInstance(context: Context): ImageDB {
            synchronized(sLock){
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        ImageDB::class.java, "ImageDB")
                        .build()
                }
                return INSTANCE!!
            }
        }

        fun destroyInstance(){
            INSTANCE = null
        }

    }
}