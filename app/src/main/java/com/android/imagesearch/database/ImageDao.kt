package com.android.imagesearch.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.android.imagesearch.model.Image

@Dao
abstract class ImageDao() {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(images: List<Image>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(image: Image): Long

    @Query("SELECT * FROM Image")
    abstract fun selectAll(): LiveData<List<Image>>

    @Query("SELECT * FROM Image WHERE title LIKE :query or name LIKE :query")
    abstract fun selectByQuery(query: String): List<Image>

    @Query("DELETE FROM Image")
    abstract fun deleteAll()
}